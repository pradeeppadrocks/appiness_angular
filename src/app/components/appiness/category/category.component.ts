import { Component, OnInit } from '@angular/core';
import { AppinessService } from '../service/appiness.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { SessionService } from 'src/app/core/services/session/session.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  categoryForm: FormGroup
  loginSubscription$: Subscription
  deleteSubscription$: Subscription
  getCategorySubscription$: any;
  createCategorySubscription$: any;
  getCategoryData: any
  addCategorySubscription$: Subscription;
  productList: any;
  orders: [];
  role: any;
  userRole: boolean;

  constructor(
    private _AppinessService: AppinessService,
    private _fb: FormBuilder,
    private _SessionService:SessionService,

  ) { }

  ngOnInit() {
    this.categoryForm = this._fb.group({
      categoryName: ['', [Validators.required]],
    })


    this.role=this._SessionService.getRole()
    if(this.role==1){
      this.userRole=true
    }else{
      this.userRole=false
    }

    this.getCategory()
  }

  onSubmit() {
    if (this.categoryForm.valid) {
      this.loginSubscription$ = this._AppinessService.createCategory(this.categoryForm.value).subscribe((res) => {
        if(res.code==409){
          alert('category already exists try different one')
        }else{
          this.getCategory()
        }
      }, (err: any) => {
      })
    } else {
      const keysSignIn = Object.keys(this.categoryForm.value);
      keysSignIn.forEach(val => {
        const ctrl = this.categoryForm.controls[val];
        if (!ctrl.valid) {
          ctrl.markAsTouched();
        };
      });
    }
  }


  getCategory() {
    this.getCategorySubscription$ = this._AppinessService.getCategory().subscribe((res) => {
      this.getCategoryData = res.category
      this.orders = res.category

    }, (err: any) => {
    })
  }

  deleteCategory(id:any) {
    this.addCategorySubscription$ = this._AppinessService.deleteCategoryOfProducts(id).subscribe((res: any) => {
      alert('Its associated product also deleted')
      this.getCategory()
    }, (err: any) => {
    })

  }
}
