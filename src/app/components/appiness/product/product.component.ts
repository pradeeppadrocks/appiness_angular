import { Component, OnInit } from '@angular/core';
import { AppinessService } from '../service/appiness.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { SessionService } from 'src/app/core/services/session/session.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  getAllProductSubscription$: any;
  getAllProductData: any;
  orders = [];
  getCategorySubscription$: any;
  addCategorySubscription$: any;
  categoryForm: FormGroup
  categoryForm1: FormGroup;
  productList: Object;
  aaaaa: boolean;
  count = 0
  role: any;
  userRole: boolean;

  constructor(
    private _AppinessService: AppinessService,
    private _fb: FormBuilder,
    private _SessionService: SessionService,
  ) { }

  ngOnInit() {
    this.categoryForm = this._fb.group({
      categoryId: [''],
      name: ['', [Validators.required]],
      price: ['', [Validators.required]],
    })

    this.categoryForm1 = this._fb.group({
      categoryId: [''],
    })

    this.role = this._SessionService.getRole()
    if (this.role == 1) {
      this.userRole = true
    } else {
      this.userRole = false
    }
    this.getAllProducst()
    this.getOrders();

  }

  onSubmit() {
    if (this.categoryForm.valid) {
      this.addCategorySubscription$ = this._AppinessService.AddProduct(this.categoryForm.value).subscribe((res) => {
        if(res.code==409){
          alert('product already exists try different one')
        }else{
          this.getAllProducst()
        }
      }, (err: any) => {
      })
    } else {
      const keysSignIn = Object.keys(this.categoryForm.value);
      keysSignIn.forEach(val => {
        const ctrl = this.categoryForm.controls[val];
        if (!ctrl.valid) {
          ctrl.markAsTouched();
        };
      });
    }
  }


  onSubmit1() {
    if (this.categoryForm1.valid) {
      this.addCategorySubscription$ = this._AppinessService.getCategoryOfProducts(this.categoryForm1.value.categoryId).subscribe((res: any) => {
        this.productList = res.order
        this.aaaaa = true
        this.count = res.count
      }, (err: any) => {
      })
    } else {
      const keysSignIn = Object.keys(this.categoryForm1.value);
      keysSignIn.forEach(val => {
        const ctrl = this.categoryForm1.controls[val];
        if (!ctrl.valid) {
          ctrl.markAsTouched();
        };
      });
    }
  }



  getOrders() {
    this.getCategorySubscription$ = this._AppinessService.getCategory().subscribe((res) => {
      this.orders = res.category
    }, (err: any) => {
    })
  }

  getAllProducst() {
    this.getAllProductSubscription$ = this._AppinessService.getAllProduct().subscribe((res) => {
      this.getAllProductData = res.products
    }, (err: any) => {
    })
  }

}
