import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/core/services/http/http.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AppinessService {

  
  private url = "http://localhost:3000/"
  constructor(private _http: HttpService,
    private htttp2:HttpClient

    
    ) { }
  getCategory() {
    return this._http.get(`${this.url}category`);
  }

  createCategory(params: object){
    return this._http.post(`${this.url}category`,params);
  }

  getAllProduct() {
    return this._http.get(`${this.url}products`);
  }

  AddProduct(params: object) {
    return this._http.post(`${this.url}products`,params);
  }

  getCategoryOfProducts(id:number){
    return this.htttp2.get(`${this.url}category/${id}`);
  }

  deleteCategoryOfProducts(id:number){
    return this.htttp2.delete(`${this.url}category/${id}`);
  }

}
