import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../services/dashboard.service';
import { SessionService } from 'src/app/core/services/session/session.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  getProductSubscription$: any;
  role: any;
  userRole: any;

  constructor(
    private _SessionService:SessionService,

  ) { }

  ngOnInit() {
     this.role=this._SessionService.getRole()
    if(this.role==1){
      this.userRole="Admin"
    }else{
      this.userRole="User"
    }
  }
}
