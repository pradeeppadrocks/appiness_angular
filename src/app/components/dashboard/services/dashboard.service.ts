import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpService } from 'src/app/core/services/http/http.service';

@Injectable()
export class DashboardService {

  private url = "http://localhost:3000/products"
  constructor(private _http: HttpService) { }
  getProduct() {
    return this._http.get(this.url);
  }

}
