import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { NoauthGuard } from 'src/app/core/guards/noauth/noauth.guard';
import { SignupComponent } from './signup/signup.component';

const routes: Routes = [
  {
    path:'login',
    component:LoginComponent,
    canActivate:[NoauthGuard],
    data: {
      title: "Login"
    }
  },
  {
    path:'signup',
    component:SignupComponent,
    canActivate:[NoauthGuard],
    data: {
      title: "signup"
    }
  },
  
  {
    path: '',
    redirectTo: 'login'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
