import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { LoginService } from '../service/login.service';
import { SessionService } from 'src/app/core/services/session/session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup
  loginSubscription$: Subscription
  returnUrl: string;
  errMsg: any;

  constructor(
    private _router: Router,
    private _fb: FormBuilder,
    private _loginService: LoginService,
    private _SessionService: SessionService,
    private _ActivatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loginForm = this._fb.group({
      email: ['', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
      password: [null, [Validators.required]],
    })
    this.returnUrl = this._ActivatedRoute.snapshot.queryParams['returnUrl'] || '/Dashboard';
  }

  onSubmit() {
    if (this.loginForm.valid) {
      this.loginSubscription$ = this._loginService.login(this.loginForm.value).subscribe((res) => {
        let isSet = this._SessionService.setSession(res.data);
        this.errMsg=res.message

        console.log(" this.errMsg", this.errMsg)
        if (isSet) {
          this._router.navigateByUrl(this.returnUrl);
        }

        setTimeout(() => {
          this.errMsg=""

        }, 4000);

      }, (err: any) => {
      })
    } else {
      const keysSignIn = Object.keys(this.loginForm.value);
      keysSignIn.forEach(val => {
        const ctrl = this.loginForm.controls[val];
        if (!ctrl.valid) {
          ctrl.markAsTouched();
        };
      });
    }
  }

}
