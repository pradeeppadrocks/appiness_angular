import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/core/services/http/http.service';

@Injectable()
export class LoginService {

  private url = "http://localhost:3000/user/"
  constructor(private _http: HttpService) { }

  login(params: object) {
    return this._http.post(`${this.url}login`, params);
  }

  signup(params: object) {
    return this._http.post(`${this.url}signup`, params);
  }

}
