import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionService } from '../../services/session/session.service';
import { TitleService } from '../../services/title/title.service';

@Injectable({
  providedIn: 'root'
})
export class NoauthGuard implements CanActivate {
  constructor(
    // private titleUrl: TitleService,
    private sess: SessionService,
    private router: Router,
    private titleUrl: TitleService,
    ) {
  }
  setPageName(currentUrl: string) {
    this.titleUrl.setPageTitle(currentUrl)
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    this.setPageName(next.data.title);

    if (!this.sess.checkLogin()) {
      return true;
    } else {
      this.router.navigateByUrl("/Dashboard");
    }
    return false;
  }
}
