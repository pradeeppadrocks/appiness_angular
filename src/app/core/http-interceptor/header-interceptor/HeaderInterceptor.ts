import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SessionService } from '../../services/session/session.service';

@Injectable()
export class HeaderInterceptor implements HttpInterceptor {

    constructor(
        private _SessionService:SessionService
    ) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let headers = {
            setHeaders: {
                'Content-Type': 'application/json'
            }
        }
         if (this._SessionService.getToken() != null) {
            headers.setHeaders['Authorization'] = 'Bearer ' + this._SessionService.getToken()
        }
        const authReq = req.clone(headers);

        return next.handle(authReq);
    }
}
