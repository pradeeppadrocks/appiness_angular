import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class HttpService {

  constructor(
    private http: HttpClient,
  ) { }


  get(url: string, options?: any, header?: object): Observable<any> {
    const params = this.buildParams(options);
    return this.http.get(url, { params })
      .pipe(
        map(res => {
          return res;
        }),
        catchError((error: HttpErrorResponse) => this.err(error))
      )
  }
  post(url: string, options: object, header?: object): Observable<any> {
    return this.http.post(url, options, header)
      .pipe(
        map(res => {
          return res;
        }),
        catchError((error: HttpErrorResponse) => this.err(error))
      );
  }


    /**
   * Converts to query string
   * @param { object } params - its prequires object which needs to be converted to query string
   * @returns strings of query params, eg., user=abcd&todo=delete
   */
  private buildParams(params: any) {
    const outputParams = new HttpParams();
    for (var key in params) {
      outputParams.set(key, params[key]);
    }
    return params || {};
  }

  err(error: HttpErrorResponse) {
    return "Something Went Wrong"
  }


}
