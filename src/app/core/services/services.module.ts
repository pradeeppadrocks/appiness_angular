import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SessionService, TitleService, HttpService } from './';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers:[
    SessionService,TitleService,HttpService
  ]
})
export class ServicesModule { }
